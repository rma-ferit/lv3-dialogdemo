package hr.ferit.brunozoric.dialog

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_main.*

const val TAG = "RMA_Dialogs"

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setUpUi()
    }

    private fun setUpUi() {
        dialogAction.setOnClickListener{displayDialog()}
    }

    private fun displayDialog() {
        AlertDialog.Builder(this)
            .setTitle(R.string.dialogTitle)
            .setMessage(R.string.dialogMessage)
            .setIcon(R.mipmap.ic_launcher)
            .setPositiveButton(android.R.string.yes){ dialog, which ->
                Log.d(TAG, "POSITIVE")
            }
            .setNegativeButton(android.R.string.no) { dialog, which ->
                Log.d(TAG, "NEGATIVE")
            }
            .create()
            .show()
    }
}
